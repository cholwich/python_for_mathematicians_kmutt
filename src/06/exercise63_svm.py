# -*- coding: utf-8 -*-
"""
Created on Thu Jul 20 15:40:38 2017

@author: gait
"""

from sklearn.datasets import load_digits
from sklearn.model_selection import KFold, GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.svm import SVC
import numpy as np

if __name__ == '__main__':
    dataset = load_digits()
    X = dataset.data
    y = dataset.target
    print(X.shape)

    model = SVC()
    outercv = KFold(n_splits=10, shuffle=True, random_state=1)
    innercv = KFold(n_splits=10, shuffle=True, random_state=1)
    acc = np.empty((10,))
    for i, (train, test) in enumerate(outercv.split(X)):
        params = {'kernel': ['linear', 'rbf'],
                  'C': [0.1, 1, 10, 100, 1000]}
        gcv = GridSearchCV(model, params, cv=innercv, verbose=1, n_jobs=-1)
        gcv.fit(X[train], y[train])

        best_C = gcv.best_params_['C']
        best_kernel = gcv.best_params_['kernel']
        print("Round %d: best_kernel = %s, best_C = %f" %
              (i, best_kernel, best_C))
        m = SVC(C=best_C, kernel=best_kernel)
        m.fit(X[train], y[train])
        y_p = m.predict(X[test])
        acc[i] = accuracy_score(y_p, y[test])
        print("Accuracy %.4f" % acc[i])
    print("%.4f" % np.mean(acc))
