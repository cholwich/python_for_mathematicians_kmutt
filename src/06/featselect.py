from sklearn.datasets import load_digits
from sklearn.feature_selection import \
    VarianceThreshold, SelectKBest, f_classif, RFECV
from sklearn.linear_model import LinearRegression


dataset = load_digits()
X, y = dataset.data, dataset.target

print("Before VarianceThreshold:", X.shape)
Xn1 = VarianceThreshold(threshold=0.01).fit_transform(X)
print("After VarianceThreshold: ", Xn1.shape)

Xn2 = SelectKBest(
    f_classif,
    k=int(0.8 * Xn1.shape[1])).fit_transform(Xn1, y)
print("After SelectKBest:       ", Xn2.shape)

Xn3 = RFECV(LinearRegression()).fit_transform(Xn1, y)
print("After RFECV:             ", Xn3.shape)
