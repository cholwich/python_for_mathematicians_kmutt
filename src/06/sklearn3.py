import sklearn.datasets
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score, mean_squared_error

dataset = sklearn.datasets.load_boston()
X = dataset.data
y = dataset.target
model = LinearRegression()
model.fit(X, y)
y_p = model.predict(X)
r2 = r2_score(y, y_p)
mse = mean_squared_error(y, y_p)
print("r2 = %.4f\tmse = %.4f" % (r2, mse))
