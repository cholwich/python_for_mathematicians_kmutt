import sklearn.datasets
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score, confusion_matrix

from sklearn.svm import SVC

dataset = sklearn.datasets.load_iris()
X = dataset.data
y = dataset.target

#print(X.shape)
#print(y.shape)

# y   = actual output
# y_p = predicted output 

#model = KNeighborsClassifier(n_neighbors=11, n_jobs=-1)
model = SVC(kernel="poly", degree=3)
model.fit(X, y)
y_p = model.predict(X)
acc = accuracy_score(y, y_p)
c = confusion_matrix(y, y_p)
print("Accuracy = %.4f" % acc)
