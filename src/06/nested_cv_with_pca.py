import numpy as np
from sklearn.datasets import load_iris
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import KFold
from sklearn.decomposition import PCA

dataset = load_iris()
X, y = dataset.data, dataset.target
n_folds = 10
outer_acc = np.empty((n_folds))
outer_cv = KFold(n_splits=n_folds, shuffle=True,
                 random_state=28020111)
for i, (outer_train, outer_test) in \
        enumerate(outer_cv.split(X)):
    inner_cv = KFold(n_splits=n_folds, shuffle=True,
                     random_state=28020111)
    best_acc = 0.0
    best_k = 0
    best_c = 0
    for n_components in range(1,4):
        for k in range(5, 20):
            inner_acc = np.empty((n_folds))
            Xn = X[outer_train]
            yn = y[outer_train]
            for j, (inner_train, inner_test) in \
                    enumerate(inner_cv.split(Xn)):
                pca = PCA(n_components=n_components)
                Xp_train = pca.fit_transform(Xn[inner_train])
                Xp_test = pca.transform(Xn[inner_test])
                model = KNeighborsClassifier(n_neighbors=k)
                model.fit(Xp_train, yn[inner_train])
                y_p = model.predict(Xp_test)
                inner_acc[j] = accuracy_score(yn[inner_test],
                                              y_p)
            m = np.mean(inner_acc)
            if m > best_acc:
                best_acc = m
                best_k = k
                best_c = n_components
    print("Round %d: best_k = %d, best_c = %d" % (i, best_k, best_c))
    pca = PCA(n_components=best_c)
    X_train = pca.fit_transform(X[outer_train])
    X_test = pca.transform(X[outer_test])
    model = KNeighborsClassifier(n_neighbors=best_k)
    model.fit(X_train, y[outer_train])
    y_p = model.predict(X_test)
    outer_acc[i] = accuracy_score(y[outer_test], y_p)
print("Accuracy = %.4f" % np.mean(outer_acc))
