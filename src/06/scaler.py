import sklearn.datasets
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.preprocessing import StandardScaler

dataset = sklearn.datasets.load_boston()
X, y = dataset.data, dataset.target
X = StandardScaler().fit_transform(X)
model = KNeighborsRegressor(n_neighbors=5, n_jobs=2)
model.fit(X, y)
y_p = model.predict(X)
r2 = r2_score(y, y_p)
mse = mean_squared_error(y, y_p)
print("r2 = %.4f\tmse = %.4f" % (r2, mse))
