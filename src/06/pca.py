import sklearn.datasets
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

dataset = sklearn.datasets.load_iris()
X, y = dataset.data, dataset.target
X = PCA(n_components=2).fit_transform(X)
model = KNeighborsClassifier(n_neighbors=5, n_jobs=2)
model.fit(X, y)
y_p = model.predict(X)
acc = accuracy_score(y, y_p)
print("Accuracy = %.4f" % acc)
