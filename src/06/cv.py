import numpy as np
from sklearn.datasets import load_iris
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import KFold, cross_val_score
from sklearn.svm import SVC

dataset = load_iris()
X, y = dataset.data, dataset.target
n_folds = 10
acc = np.empty((n_folds))
cv = KFold(n_splits=n_folds, shuffle=True,
           random_state=28020111)
#for i, (train, test) in enumerate(cv.split(X)):
#    model = KNeighborsClassifier(n_neighbors=5, n_jobs=2)
#    model = SVC()
#    model.fit(X[train], y[train])
#    y_p = model.predict(X[test])
#    acc[i] = accuracy_score(y[test], y_p)
#    print("Round %d => %.4f" % (i, acc[i]))
#print("Accuracy = %.4f" % np.mean(acc))

model = SVC()
acc = cross_val_score(model, X, y, cv=cv, n_jobs=1)
print(acc)