import numpy as np
from sklearn.datasets import load_iris
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import KFold, GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA

dataset = load_iris()
X, y = dataset.data, dataset.target
n_folds = 10
outer_acc = np.empty((n_folds))
outer_cv = KFold(n_splits=n_folds, shuffle=True,
                 random_state=28020111)
for i, (outer_train, outer_test) in \
        enumerate(outer_cv.split(X)):
    parameters = {'knn__n_neighbors': np.arange(5, 20),
                  'pca__n_components' : np.arange(1, 4)}
    inner_cv = KFold(n_splits=n_folds,
                     shuffle=True,
                     random_state=28020111)
    pipeline = Pipeline([("pca", PCA()), 
                         ("knn", KNeighborsClassifier())])
    inner = GridSearchCV(pipeline,
                         parameters,
                         cv=inner_cv)
    inner.fit(X[outer_train], y[outer_train])
    best_k = inner.best_params_['knn__n_neighbors']
    best_c = inner.best_params_['pca__n_components']
    print("Round %d: best_k=%d, best_c=%d" % (i, best_k, best_c))
    pca = PCA(n_components=best_c)
    X_train = pca.fit_transform(X[outer_train])
    X_test = pca.transform(X[outer_test])
    model = KNeighborsClassifier(n_neighbors=best_k)
    model.fit(X_train, y[outer_train])
    y_p = model.predict(X_test)
    outer_acc[i] = accuracy_score(y[outer_test], y_p)
print("Accuracy = %.4f" % np.mean(outer_acc))
