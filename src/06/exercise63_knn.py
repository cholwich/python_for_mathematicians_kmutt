# -*- coding: utf-8 -*-
"""
Created on Thu Jul 20 15:40:38 2017

@author: gait
"""

import numpy as np
from sklearn.datasets import load_digits
from sklearn.model_selection import KFold, GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score


if __name__ == '__main__':
    dataset = load_digits()
    X = dataset.data
    y = dataset.target
    print(X.shape)

    model = KNeighborsClassifier(n_jobs=-1)
    outercv = KFold(n_splits=10, shuffle=True, random_state=1)
    innercv = KFold(n_splits=10, shuffle=True, random_state=1)
    acc = np.empty((10,))
    for i, (train, test) in enumerate(outercv.split(X)):
        params = {'n_neighbors': np.arange(1, 11)}
        gcv = GridSearchCV(model, params, cv=innercv, verbose=1, n_jobs=-1)
        gcv.fit(X[train], y[train])

        best_k = gcv.best_params_['n_neighbors']
        print("Round %d: best_k = %d" % (i, best_k))
        m = KNeighborsClassifier(n_neighbors=best_k, n_jobs=-1)
        m.fit(X[train], y[train])
        y_p = m.predict(X[test])
        acc[i] = accuracy_score(y_p, y[test])
    print("%.4f" % np.mean(acc))
