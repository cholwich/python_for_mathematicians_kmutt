import sklearn.datasets
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.svm import SVR
import matplotlib.pyplot as plt

dataset = sklearn.datasets.load_boston()
X = dataset.data
y = dataset.target
#model = KNeighborsRegressor(n_neighbors=5, n_jobs=2)
model = SVR(kernel='poly')
model.fit(X, y)
y_p = model.predict(X)
r2 = r2_score(y, y_p)
mse = mean_squared_error(y, y_p)
print("r2 = %.4f\tmse = %.4f" % (r2, mse))

fig = plt.figure()
a = fig.add_subplot(1,1,1)
a.scatter(y, y_p)
plt.show()