import numpy as np
import matplotlib.pyplot as plt

np.random.seed(65535)
X1 = np.random.normal(loc=(0, 1, 2, 1),
                      scale=(1.0, 0.5, 2.0, 0.2),
                      size=(500, 4))
X2 = np.random.normal(loc=(3, 2, 5, 3),
                      scale=(0.7, 1.0, 0.8, 0.1),
                      size=(500, 4))

fig = plt.figure()
ax = fig.add_subplot(111)
ax.scatter(X1[:, 0], X1[:, 1], color="red")
ax.scatter(X2[:, 0], X2[:, 1], color="blue")

plt.show()
