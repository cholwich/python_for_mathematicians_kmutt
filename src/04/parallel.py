import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas.plotting import parallel_coordinates

np.random.seed(65535)
X1 = np.random.normal(loc=(0, 1, 2, 1),
                      scale=(1.0, 0.5, 2.0, 0.2),
                      size=(500, 4))
X2 = np.random.normal(loc=(3, 2, 5, 3),
                      scale=(0.7, 1.0, 0.8, 0.1),
                      size=(500, 4))

X1 = np.c_[X1, np.ones((500))]
X2 = np.c_[X2, 2 * np.ones((500))]
X = np.vstack((X1, X2))

D=pd.DataFrame(X)

plt.figure()
parallel_coordinates(D, 4)
plt.show()
