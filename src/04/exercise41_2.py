# -*- coding: utf-8 -*-
"""
Created on Wed Jul 19 11:28:19 2017

@author: gait
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA

D = pd.read_csv('iris.csv', header=None).as_matrix()
print(D.shape)

i1 = np.where(D=='Iris-setosa')[0]
D1 = D[i1, :]

i2 = np.where(D=='Iris-versicolor')[0]
D2 = D[i2, :]

i3 = np.where(D=='Iris-virginica')[0]
D3 = D[i3, :]

pca = PCA(n_components=2)
pca.fit(D[:, :4])

D1n = pca.transform(D1[:, :4])
D2n = pca.transform(D2[:, :4])
D3n = pca.transform(D3[:, :4])

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.scatter(D1n[:, 0], D1n[:, 1], color="red")
ax.scatter(D2n[:, 0], D2n[:, 1], color="blue")
ax.scatter(D3n[:, 0], D3n[:, 1], color="green")
        
plt.show()