import numpy as np
import matplotlib.pyplot as plt

np.random.seed(65535)
X1 = np.random.normal(loc=(0, 1, 2, 1),
                      scale=(1.0, 0.5, 2.0, 0.2),
                      size=(500, 4))
X2 = np.random.normal(loc=(3, 2, 5, 3),
                      scale=(0.7, 1.0, 0.8, 0.1),
                      size=(500, 4))

fig = plt.figure(figsize=(8, 8))
for i in range(4):
    for j in range(4):
        index = i * 4 + j + 1
        ax = fig.add_subplot(4, 4, index)
        ax.scatter(X1[:, i], X1[:, j], color="red")
        ax.scatter(X2[:, i], X2[:, j], color="blue")

plt.show()
