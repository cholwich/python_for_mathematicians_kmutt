# -*- coding: utf-8 -*-
"""
Created on Wed Jul 19 11:28:19 2017

@author: gait
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from pandas.plotting import parallel_coordinates

D = pd.read_csv('iris.csv', header=None)

fig = plt.figure()
parallel_coordinates(D, 4)        
plt.show()