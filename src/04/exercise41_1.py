# -*- coding: utf-8 -*-
"""
Created on Wed Jul 19 11:28:19 2017

@author: gait
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

D = pd.read_csv('iris.csv', header=None).as_matrix()
print(D.shape)

i1 = np.where(D=='Iris-setosa')[0]
D1 = D[i1, :]

i2 = np.where(D=='Iris-versicolor')[0]
D2 = D[i2, :]

i3 = np.where(D=='Iris-virginica')[0]
D3 = D[i3, :]

fig = plt.figure()
for i in range(4):
    for j in range(4):
        index = (i*4+j)+1
        ax = fig.add_subplot(4,4,index)
        ax.scatter(D1[:, i], D1[:, j], color="red")
        ax.scatter(D2[:, i], D2[:, j], color="blue")
        ax.scatter(D3[:, i], D3[:, j], color="green")
        
plt.show()