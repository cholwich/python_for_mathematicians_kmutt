import numpy as np
import matplotlib.pyplot as plt
import matplotlib

X = np.array([[1, 15], [2, 30], [3, 10],
              [4, 7], [6, 8]])

Z = np.array([[0, 1, 5], [5, 2, 50], [8, 7, 27]])

fig = plt.figure()
ax1 = fig.add_subplot(1, 2, 1)  # numrows,numcols,index
ax1.plot(X[:, 0], X[:, 1], 'o-', color='blue')

ax2 = fig.add_subplot(1, 2, 2)

cm = plt.get_cmap('bwr')
ax2.scatter(X[:, 0], X[:, 1], color='green')
normalize = matplotlib.colors.Normalize(vmin=0., vmax=1.)
ax2.scatter(Z[:, 0], Z[:, 1], c=Z[:, 2] / 50,
            cmap=cm, norm=normalize, s=100)

plt.savefig("basic_py.pdf", format='pdf')
plt.show()
