from sympy import *
from IPython.display import display

# init_session()
x, y = symbols("x y")

expr = exp(x**2)

display(diff(expr, x))
display(diff(expr, x, x))

d = Derivative(expr)
display(d)
display(d.doit())

expr = exp(-x**2-y**2) 
i = Integral(expr, (x, -oo, oo), (y, -oo, oo))
display(i)
display(i.doit())

expr = Limit(1/x, x, 0, '-')
display(expr)
display(expr.doit())