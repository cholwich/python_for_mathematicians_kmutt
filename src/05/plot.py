from sympy import *
from sympy.plotting import plot, plot3d

x = symbols("x")

plot(x, x**2, (x, -2, 2))

plot3d(x**2 + y**2, (x, -5, 5), (y, -5, 5))
