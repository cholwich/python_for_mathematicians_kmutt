from sympy import *
from IPython.display import display

x, y = symbols("x y")
expr = (x + y)**2
# ASCII pretty print
pprint(expr)
# LaTeX output
display(expr)
# Print LaTeX source code
print(latex(expr))
