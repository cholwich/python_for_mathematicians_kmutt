from sympy import *

x, y = symbols("x y")

e = Eq(x, x**2 - 3 * x + 4)
pprint(solveset(e, x))

pprint(solveset(exp(x), x))
