# -*- coding: utf-8 -*-
"""
Created on Wed Jul 19 14:33:27 2017

@author: gait
"""

from sympy import *
from sympy.plotting import plot

x = symbols('x')
e = x**4 - 6 * x**2 + 4
# e = (x - 6)**2 + 5
d1 = diff(e, x)
d2 = diff(e, x, x)

l = list(solveset(d1, x))
print("Critical points:", l)
cx = []
cv = []
for i in l:
    j = d2.subs(x, i)
    if j > 0:
        cx.append(i)
        cv.append(e.subs(x, i))

cx.append(oo)
cv.append(Limit(e, x, oo).doit())

cx.append(-oo)
cv.append(Limit(e, x, -oo).doit())

print("X:", cx)
print("V:", cv)

plot(e, (x,-3,3))