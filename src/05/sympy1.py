import sympy
import math

print("math.sqrt(9)  =", math.sqrt(9))
print("sympy.sqrt(9) =", sympy.sqrt(9))

print("math.sqrt(8)  =", math.sqrt(8))
print("sympy.sqrt(8) =", sympy.sqrt(8))
