from sympy import *


x, y, z = symbols("x y z")

expr = (x**3 + x**2 - x - 1) / (x**2 + 2 * x + 1)

pprint(simplify(expr))
pprint(expand(expr))

expr = cos(x)**2 + 2 * cos(x) * sin(x) + sin(x)**2
pprint(factor(expr))
print(latex(factor(expr)))
