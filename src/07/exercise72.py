import os
import numpy as np
import tensorflow as tf
import time

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

X = np.load('exercise31.npy')

start_time = time.time()

n_dim = X.shape[1]
n_rows = X.shape[0]

print("Preparing computational graph")
A = tf.placeholder(tf.float64, shape=(None, n_dim))
r = tf.reduce_sum(A * A, 1)
r = tf.reshape(r, [-1, 1])
D = r - 2 * tf.matmul(A, tf.transpose(A)) + tf.transpose(r)
m = tf.reduce_max(D)
D = tf.clip_by_value(D, 0., m)
D = tf.sqrt(D)

init = tf.global_variables_initializer()

print("Start Executing")
sess = tf.Session()
sess.run(init)
d = sess.run(D, {A: X})

print("Execution time: ", time.time() - start_time)

print(d)
np.save('tfdist.npy', d)
