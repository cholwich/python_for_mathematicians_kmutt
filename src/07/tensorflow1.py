import os
import math
import numpy as np
import tensorflow as tf

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

x = tf.constant(math.pi)
s = tf.sin(x)**2
t = tf.cos(x)**2
u = s + t

print(x, s)
print(t, u)

sess = tf.Session()
r_u = sess.run(u)
print(r_u)
