import os
import numpy as np
from tensorflow.contrib.keras.api.keras.layers import Input, Dense, Dropout
from tensorflow.contrib.keras.api.keras.models import Model
from tensorflow.contrib.keras.api.keras.callbacks import EarlyStopping, \
    ModelCheckpoint

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

D = np.load('digits.npy')
X = D[:, :-1]
y = D[:, -1].astype(int)

np.random.seed(28021101)
n_rows = X.shape[0]

# Normalize values
X = X / 16.
# Prepare y
Y = np.zeros((n_rows, 10))
Y[np.arange(n_rows), y] = 1.

idx = np.arange(n_rows)
np.random.shuffle(idx)
tidx = int(0.8 * n_rows)
X = X[idx, :]
Y = Y[idx, :]
X_test = X[tidx:, :]
Y_test = Y[tidx:, :]
X = X[:tidx, :]
Y = Y[:tidx, :]

inputs = Input(shape=(64,))

x = Dense(512, activation='relu')(inputs)
x = Dropout(0.5)(x)
x = Dense(512, activation='relu')(x)
x = Dropout(0.5)(x)

outputs = Dense(10, activation='softmax')(x)

model = Model(inputs=inputs, outputs=outputs)
model.compile(optimizer='rmsprop',
              loss='categorical_crossentropy',
              metrics=['accuracy'])

earlyStopping = EarlyStopping(monitor='val_loss',
                              patience=30,
                              verbose=0,
                              mode='auto')
modelCheckpoint = ModelCheckpoint('best_weights.h5',
                                  save_best_only=True,
                                  save_weights_only=True)
model.fit(X, Y,
          batch_size=128,
          epochs=1000,
          validation_split=0.1,
          callbacks=[earlyStopping, modelCheckpoint])

r = model.evaluate(X_test, Y_test, verbose=0)
print(r)
