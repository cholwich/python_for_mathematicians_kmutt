# -*- coding: utf-8 -*-
"""
Created on Fri Jul 21 10:44:46 2017

"""
import os
import tensorflow as tf

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

print("Input x:", end="")
x = float(input())
print("Input y:", end="")
y = float(input())

x_t = tf.placeholder(tf.float32)
y_t = tf.placeholder(tf.float32)
s = tf.sqrt(x_t**2 + y_t**2)

sess = tf.Session()
r_s = sess.run(s, {x_t:x, y_t:y})
print(r_s)