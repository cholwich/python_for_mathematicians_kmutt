import os
import math
import numpy as np
import tensorflow as tf

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

v1 = tf.Variable([0., 0., 0.], dtype=tf.float32)
x = tf.placeholder(tf.float32)
y = v1 + x
y = tf.assign(v1, y)

sess = tf.Session()
init = tf.global_variables_initializer()

sess.run(init)
r_y = sess.run(y, {x: [5., 3., 1.]})
print(r_y)

r_y = sess.run(y, {x: [2., 2., 5.]})
print(r_y)