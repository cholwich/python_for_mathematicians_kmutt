import os
import numpy as np
# import tensorflow.contrib.keras as keras
from tensorflow.contrib.keras.api.keras.layers import Input, Dense
from tensorflow.contrib.keras.api.keras.models import Model

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# training examples
tx = np.array([[0, 0], [0, 1],
               [1, 0], [1, 1]]).astype('float32')
ty = np.array([0, 1, 1, 0]).astype('float32')

# define the input layer
inputs = Input(shape=(2,))

# define a hidden layer with 8 rectified linear units (ReLU)
# the ReLU is defined as g(x) = max{0, x}
x = Dense(8, activation='relu',
          kernel_initializer='uniform')(inputs)

# define the output layer with 1 sigmoid unit
outputs = Dense(1, activation='sigmoid',
                kernel_initializer='uniform')(x)

model = Model(inputs=inputs, outputs=outputs)
model.compile(optimizer='rmsprop',
              loss='binary_crossentropy',
              metrics=['accuracy'])

model.fit(tx, ty, epochs=1000)

print(model.predict(tx))
