import numpy as np
import time

X = np.load('exercise31.npy')
# X = np.array([[1,1,1],[2,2,2],[2,1,2]])

start_time = time.time()

r = np.sum(X*X, axis=1)

r = r.reshape((-1, 1))
A = np.matmul(X, X.T)
D = r - 2 * A + r.T
D[np.isclose(D, 0.0)] = 0.0
D = np.sqrt(D)

print("Execution time:", (time.time()-start_time))

Dn = np.load('dist.npy')
print(np.max(np.abs(D-Dn)))