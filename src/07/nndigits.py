import os
import tensorflow as tf
import numpy as np

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

D = np.load('digits.npy')
X = D[:, :-1]
y = D[:, -1].astype(int)

np.random.seed(28021101)
n_rows = X.shape[0]

# Normalize values
X = X / 16.
# Prepare y
Y = np.zeros((n_rows, 10))
Y[np.arange(n_rows), y] = 1.

idx = np.arange(n_rows)
np.random.shuffle(idx)
tidx = int(0.8 * n_rows)
X_test = X[tidx:, :]
Y_test = Y[tidx:, :]
X = X[:tidx, :]
Y = Y[:tidx, :]

x_ = tf.placeholder(tf.float32, [None, 64])
y_ = tf.placeholder(tf.float32, [None, 10])
W = tf.Variable(tf.zeros([64, 10]))
b = tf.Variable(tf.zeros([10]))
y_p = tf.nn.softmax(tf.matmul(x_, W) + b)

cross_entropy = tf.reduce_mean(
    -tf.reduce_sum(y_ * tf.log(y_p),
                   reduction_indices=[1]))

train_step = tf.train.GradientDescentOptimizer(
    0.05).minimize(cross_entropy)

sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)
batch_size = 64
for i in range(1000):
    print("Training Round %d" % i)
    idx = np.arange(X.shape[0])
    np.random.shuffle(idx)
    idx = idx[:batch_size]
    X_batch = X[idx, :]
    Y_batch = Y[idx, :]

    sess.run(train_step, {x_: X_batch, y_: Y_batch})

correct_prediction = tf.equal(tf.argmax(y_, 1),
                              tf.argmax(y_p, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction,
                                  tf.float32))
print(sess.run(accuracy, {x_: X_test, y_: Y_test}))
