print("Input x: ", end="")
x = int(input())

if x > 100:
    print("Too large")
    print("Your input value is %d" % x)
elif x > 50:
    print("Quite large")
else:
    print("Not large")
