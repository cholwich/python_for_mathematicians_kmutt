list2 = ["sin", "cos", "tan", "sec"]

print(list2[0])     # "sin"
print(list2[-1])    # "sec"
print(list2[1:-1])  # ["cos", "tan"]
