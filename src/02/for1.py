for x in [2, 3, 5, 7]:
    print("%4d" % x, end="")
print()

sum = 0
for i in range(100):
    sum += i
print("Sum = %d" % sum)
