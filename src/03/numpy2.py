import numpy as np

e = np.arange(5)
print("e=%s" % e)

f = np.arange(1, 5)
print("f=%s" % f)

g = np.arange(1, 50, 5)
print("g=%s" % g)

h = np.random.random((2, 3))
print("h=%s" % h)

i = np.random.normal(loc=0.0, scale=1.0, size=(3, 3))
print("i=%s" % i)
