import numpy as np
from scipy.optimize import minimize


def f(x):    # define a function accepting an array x
    return (x[0] - 1)**2 / 4 + (x[1] - 5)**2 / 8


def g(x):
    return 3 * x[0] + 9 * x[1]


def h(x):
    return (x[0]**0.5) * (x[1]**0.5) - 100


x0 = [(0., 0.)]
result = minimize(f, x0)
print(result)

print('-' * 20)

x0 = [(1., 1.)]
result = minimize(g, x0, method='SLSQP',
                  constraints=({'type': 'ineq',
                                'fun': h}))
print(result)
