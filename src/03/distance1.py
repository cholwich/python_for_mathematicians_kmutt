# -*- coding: utf-8 -*-
"""
Created on Tue Jul 18 10:42:52 2017

@author: gait
"""

import numpy as np
import time

X = np.load("exercise31.npy")
n = X.shape[0]

# X[i, :] # ith row of X
start_time = time.time()

D = np.zeros((n, n))

for i in range(n):
    for j in range(i + 1, n):
        d = np.sqrt(np.sum((X[i, :] - X[j, :])**2))
        D[i, j] = d
        D[j, i] = d

print("Execution time:", (time.time()-start_time))

np.save('dist.npy', D)
