import numpy as np

a = np.array([[1, 2, 3], [4, 5, 6]])

print(a[0:1, 1:])

a[0:1, 1:] = np.array([[7, 8]])
print(a)
