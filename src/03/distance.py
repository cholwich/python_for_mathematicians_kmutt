import numpy as np
from scipy.spatial.distance import *

X = np.load('exercise31.npy')

D = squareform(pdist(X, metric='euclidean'))

print(D.shape)
