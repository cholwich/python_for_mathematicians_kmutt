import numpy as np

a = np.array([[1, 0], [0, 1]])
b = np.arange(4).reshape((2, 2))

c = a + b
print("c=%s" % c)

d = a * b               # elementwise product
print("d=%s" % d)

e = np.matmul(a, b)     # matrix multiplication
print("e=%s" % e)

f = np.array([[1, 1, 1],
              [2, 3, 4]])

print("f.sum()=%d" % f.sum())
print("f.sum(axis=0)=%s" % f.sum(axis=0))
print("f.sum(axis=1)=%s" % f.sum(axis=1))

print("f.argmax()=%d" % f.argmax())
print("f.argmax(axis=0)=%s" % f.argmax(axis=0))
print("f.argmax(axis=1)=%s" % f.argmax(axis=1))

print("f.reshape((-1,1))=%s" % f.reshape((-1, 1)))
print("f.reshape((1,-1))=%s" % f.reshape((1, -1)))
print("f.ravel()=%s" % f.ravel())
