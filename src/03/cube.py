import numpy as np

C = np.array([[[1, 1, 1],
               [1, 1, 1],
               [1, 1, 1]],
              [[2, 2, 2],
               [2, 2, 2],
               [2, 2, 2]],
              [[3, 3, 3],
               [3, 3, 3],
               [3, 3, 3]],
              [[4, 4, 4],
               [4, 4, 4],
               [4, 4, 4]]])

print(C.shape)
