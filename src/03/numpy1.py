import numpy as np

a = np.array([1, 2, 3, 4])
b = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
c = np.zeros((3, 3))
d = np.ones((3, 3), dtype=np.int)
print("%s with ndim=%d, size=%d, shape=%s, dtype=%s\n" %
      (a, a.ndim, a.size, a.shape, a.dtype))
print("%s with ndim=%d, size=%d, shape=%s, dtype=%s\n" %
      (b, b.ndim, b.size, b.shape, b.dtype))
print("%s with ndim=%d, size=%d, shape=%s, dtype=%s\n" %
      (c, c.ndim, c.size, c.shape, c.dtype))
print("%s with ndim=%d, size=%d, shape=%s, dtype=%s\n" %
      (d, d.ndim, d.size, d.shape, d.dtype))
