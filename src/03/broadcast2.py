import numpy as np

a = np.ones((4, 4))     # shape = 4 x 4
b = 2                   # scalar value
print(a + b)

c = np.array([5])
print(a + c)            # shape =     1

d = np.arange(4)        # shape =     4
print(a + d)

e = d.reshape((-1, 1))  # shape = 4 x 1
print(a + e)
