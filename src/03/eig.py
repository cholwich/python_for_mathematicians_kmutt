import numpy as np

M = np.diag((1, 2, 3))  # Create a diagonal matrix

w, v = np.linalg.eig(M)
print("Eigenvalues =")
print(w)
print("Eigenvectors =")
print(v)

for i in range(M.shape[0]):
    print(w[i], v[:, i])
