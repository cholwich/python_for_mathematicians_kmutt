a = 10
print(a)
print("a", a)
print("a=%d" % a)
print("a=%5d" % a)
print("a=%05d" % a)
print("=====")
a = 2.71828
b = 3.14159
print(a)
print("a", a)
print("a=%.3f" % a)
print("a=%8.3f" % a)
print("a=%f, b=%f" % (a, b))
print("a=%.2f, b=%.2f" % (a, b))
print("=====")
a = True
print(a)
print("=====")
a = "Hello"
print(a)
print("%s, World" % a)
