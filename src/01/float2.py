import math

a = 0.1

b = a + a + a + a + a + a + a + a + a + a
c = a * 10

print("b = %.17f" % b)
print("c = %.17f" % c)

print("Comparison operator:",
      b == c)
print("math.isclose       :",
      math.isclose(b, c, rel_tol=1e-10))
