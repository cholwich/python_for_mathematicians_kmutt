from math import sin, cos, pi

a = sin(pi) + cos(pi)
print("%.2f" % a)
